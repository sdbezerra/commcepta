<?php 
//Recebe o ID do perfil enviado via jquery
$id = $_GET["id"];
//Pega os dados JSON do arquivo
$string = file_get_contents("Dados/dados.json");
$json = json_decode($string, true);
foreach($json as $dados) {
    //Verifica se o dado recebido equivale ao ID recebido
    if ($id == $dados['id']) {
        $item = $dados;
        break;
    }
}
?>
<!--Imprime as informações na index.php-->
<div class="x_content">
    <div class="col-md-3 col-sm-8 col-xs-8 profile_left">
        <div>
            <div class="profile_img col-md-6 col-sm-6 col-xs-12">
                <div id="crop-avatar">
                    <img class="img-responsive avatar-view" src="images/<?=$item['foto']?>" alt="Avatar" title="">
                </div>
            </div>
            <h3><?=$item['nome']?></h3>
            <ul class="list-unstyled user_data">
                <li><i class="fa fa-gift user-profile-icon"></i> Idade: <?=$item['idade']?>
                </li>
                <li>
                    <i class="fa fa-briefcase user-profile-icon"></i> Cargo: <?=$item['cargo']?>
                </li>
            </ul>
        </div>
        <br />
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12">
    </div>
</div>