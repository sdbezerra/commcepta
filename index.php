<?
$string = file_get_contents("Dados/dados.json");
$json = json_decode($string, true);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Commcepta Layout</title>
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="build/css/custom.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container body">
            <nav class="navbar navbar-default">

                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navposition">
                            <span class="sr-only">Alternar</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img src="/images/logo.png"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="navposition">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="individualbtnposition"><a href="/">EMPRESA</a></li>
                            <li class="individualbtnposition"><a href="/">SERVIÇO</a></li>
                            <li class="individualbtnposition"><a href="/">CONTATO</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container body">
                <div class="right_col" role="main">
                    <div>
                        <div class="clearfix"></div>
                        <!--Div destinado a receber os dados via JQUERY-->
                        <div id="detalhes" class="collapse">

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <ul class="list-inline">
                                                <!--Captura as informações do json e converte em Array-->
                                                <? foreach ($json as $nomes => $detalhe) { ?>
                                                    <li class="list-group-item col-md-3 col-sm-4 col-xs-8">
                                                        <button id="btndetalhes" data-id="<?= $detalhe['id'] ?>" style="padding: 10px 15px 15px 15px;" data-toggle="collapse" data-target="#detalhes">
                                                            <div>
                                                                <div class="foto">
                                                                    <img class="img-circle col-md-6 col-sm-4 col-xs-6" style="display: block;" src="images/<?= $detalhe['foto'] ?>" alt="image" />
                                                                    <span class="id-badge"><?= $detalhe['id'] ?></span>
                                                                </div>
                                                                <p><strong><?= $detalhe['nome'] ?></strong></p>
                                                                <p><?= $detalhe['cargo'] ?></p>
                                                            </div>
                                                        </button>
                                                    </li>
                                                <? } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <div class="pull-right">
                        Layout de captura de dados
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>
        <script src="vendors/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="build/js/custom.min.js"></script>
        <script>
            //identifica o click pegando os parâmetros dos botões
            //O Arquivo que processa os detalhes é: detalhes.php
            $('button').click(function () {
                $.ajax({
                    type: 'GET',
                    datatype: "html",
                    url: "/detalhes.php",
                    data: {id: $(this).data("id")},
                    success: function (data) {
                        $("#detalhes").html(data);
                    }
                });
            });
        </script>
    </body>
</html>